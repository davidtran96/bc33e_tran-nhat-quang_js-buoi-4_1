function loaiTamGiac() {
  var canhA = document.getElementById("txt-canh-a").value * 1;
  var canhB = document.getElementById("txt-canh-b").value * 1;
  var canhC = document.getElementById("txt-canh-c").value * 1;
  if (canhA == canhB && canhB == canhC) {
    document.querySelector("#result").innerHTML = `Đây là tam giác đều`;
  } else if (canhA == canhB || canhA == canhC || canhB == canhC) {
    document.querySelector("#result").innerHTML = `Đây là tam giác cân`;
  } else if (canhC * canhC == canhA * canhA + canhB * canhB) {
    document.querySelector("#result").innerHTML = `Đây là tam giác vuông`;
  } else {
    document.querySelector("#result").innerHTML = `Đây là tam giác bình thường`;
  }
}
