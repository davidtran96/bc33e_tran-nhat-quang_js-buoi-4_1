function demChanLe() {
  var num1Value = document.getElementById("txt-num-1").value * 1;
  var num2Value = document.getElementById("txt-num-2").value * 1;
  var num3Value = document.getElementById("txt-num-3").value * 1;
  var soChan = 0;
  var soLe = 0;
  if (num1Value % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  if (num2Value % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  if (num3Value % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  document.querySelector(
    "#result"
  ).innerHTML = `Có ${soChan} số chẵn và ${soLe} số lẻ`;
}
